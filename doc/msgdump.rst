Module :mod:`msgdump.decoder`
-----------------------------

.. automodule:: msgdump.decoder
    :members:
    :undoc-members:
    :show-inheritance:

Module :mod:`msgdump.messages`
------------------------------

.. automodule:: msgdump.messages
    :members:
    :undoc-members:
    :inherited-members:

Module :mod:`msgdump.utils`
---------------------------

.. automodule:: msgdump.utils
    :members:
    :undoc-members:
    :show-inheritance:

