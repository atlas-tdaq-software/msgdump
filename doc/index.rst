.. msgdump documentation master file, created by
   sphinx-quickstart on Mon Aug 26 17:13:04 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to msgdump's documentation!
===================================

.. automodule:: msgdump.__init__
    :members:
    :undoc-members:
    :show-inheritance:


API Documentation
-----------------

.. toctree::
   :maxdepth: 4

   msgdump


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

