#
# Test all
#

from test_messages import *
from test_decoder import *
from test_utils import *
from test_helper import *

if __name__ == '__main__':
  import sys
  import unittest
  sys.argv.append('-v')
  unittest.main()
