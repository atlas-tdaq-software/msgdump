#
# Test msgdump.decoder
#

import unittest
from msgdump import decoder, messages

from helper import Factory
from builtins import bytes

class TestDecode(unittest.TestCase):
    """Test on `DecodedPacket` returned from the `decoder.decode` function."""

    def setUp(self):
        # Packet corresponding to a FragmentRequest
        fct = Factory()
        self.raw_packet = bytes(b'\x00\xe0\xed\x1e\xc7\x0f\x00\x8c\xfa\x00\x01\xcd\x08\x00E\x00\x00h\xcb\xa6@\x00@\x06\xeb|\n\x96-C\n\x96@\xfe\xe4\xc6\xd7\xaf\xbe\xf7\xe5\x10\x92?()P\x180\x00S\xdb\x00\x00 \xdf\xdc\x00\xd9\x1b\x00\x004\x00\x00\x00\xf0?!\x02A\x02C\x00B\x02C\x00C\x02C\x00D\x02C\x00E\x02C\x00F\x02C\x00G\x02')
        self.pkt_hdr = fct.pcapy_header(self.raw_packet)

    #
    # Test headers
    #
    def test_tcp_header_(self):
        """check TCP header"""
        dec_pack = decoder.decode(self.pkt_hdr, self.raw_packet)
        self.assertIn('TCP', dec_pack.headers)
        self.assertEqual(1, dec_pack.get('ACK'))
        self.assertEqual(58566, dec_pack.get('source_port'))

    def test_ipv4_header(self):
        """check IPV4 header"""
        dec_pack = decoder.decode(self.pkt_hdr, self.raw_packet)
        self.assertIn('IPV4', dec_pack.headers)
        self.assertEqual('10.150.45.67', dec_pack.get('source_address'))

    def test_eth_header(self):
        """check ETH header"""
        dec_pack = decoder.decode(self.pkt_hdr, self.raw_packet)
        self.assertIn('ETH', dec_pack.headers)
        self.assertEqual('008cfa0001cd', dec_pack.get('source-MAC'))
        self.assertEqual(2048, dec_pack.get('ethertype'))

    #
    # Test inner message
    #
    def test_msg_type(self):
        """check message type"""
        dec_pack = decoder.decode(self.pkt_hdr, self.raw_packet)
        msg = dec_pack.msg
        self.assertIs(msg.type(), messages.FragmentRequest)

    def test_msg_payload(self):
        """check message content"""
        dec_pack = decoder.decode(self.pkt_hdr, self.raw_packet)
        msg = dec_pack.msg
        self.assertEqual(msg.L1Id(), 35733488)
        self.assertEqual(msg.ROBIds(), [4391489, 4391490, 4391491, 4391492, 4391493, 4391494, 583])

    def test_msg_header(self):
        """check message (common) header"""
        dec_pack = decoder.decode(self.pkt_hdr, self.raw_packet)
        msg = dec_pack.msg
        self.assertEqual(msg.transId, 7129)
        self.assertEqual(msg.size, 52)

    def test_decode_udp_clear(self):
        """check decode `Clear` message (with UDP protocol)"""
        fct = Factory()
        raw_packet = bytes(b'\x01\x00^d\x01\x01\x90\xe2\xba\x01\xc4\x84\x08\x00E\x00\x01\xb8\x03\x0e@\x00\x01\x11HG\n\xc1@\xba\xe0d\x01\x01\xa4K#(\x01\xa4i\x8c\x10\xdf\xdc\x00\xe8$\x00\x00\x90\x01\x00\x00\xa1j\x0e\x00\xa3j\x0e\x00\xa4j\x0e\x00\xa7j\x0e\x00\xa9j\x0e\x00bj\x0e\x00\xa5j\x0e\x00\xacj\x0e\x00\xa8j\x0e\x00\xa6j\x0e\x00\xa0j')
        pkt_hdr = fct.pcapy_header(raw_packet)
        dec_pkt = decoder.decode(pkt_hdr, raw_packet)
        self.assertIs(dec_pkt.msg.type(), messages.Clear)


class TestDecodeErrors(unittest.TestCase):

    def setUp(self):
        fct = Factory()
        # Packet NOT corresponding to a IPV4 fragment
        # 137 (0x89) == MPLS-in-IP
        self.raw_packet = bytes(b'\x01\x80\xc2\x00\x00\x00\x00\x165\xb4A\xb3\x00\x89BB\x03\x00\x00\x03\x02|\x00\x00\x00$8\xaaG\x00\x00\x00\x00\x00\x00\x00\x00$8\xaaG\x00\x80\r\x00\x00\x14\x00\x02\x00\x0f\x00\x00\x00`\x00DC1-DC2\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00D(A\x0b\xc7\xf1')
        self.pkt_hdr = fct.pcapy_header(self.raw_packet)


    def test_decode_raise(self):
        """test decode raise WrongFragment"""
        with self.assertRaises(decoder.WrongFragment):
            pkt = decoder.decode(self.pkt_hdr, self.raw_packet)


if __name__ == '__main__':
    unittest.main()
