
from msgdump import messages

import socket
import struct
from builtins import bytes

class Factory(object):

    def eth(self):
        return bytes(b'\x00\xe0\xed\x1e\xc7\x0f\x00\x8c\xfa\x00\x01\xcd\x08\x00')

    def ipv4(self, source_address=None, destination_address=None):
        '10.150.45.67'
        fragment = bytes(b'E\x00\x00h\xcb\xa6@\x00@\x06\xeb|\n\x96-C\n\x96@\xfe')
        if source_address:
            fragment = fragment[:12] + socket.inet_aton(source_address) + fragment[16:]
        if destination_address:
            fragment = fragment[:16] + socket.inet_aton(destination_address) + fragment[20:]
        return fragment

    def tcp(self):
        return bytes(b'\xe4\xc6\xd7\xaf\xbe\xf7\xe5\x10\x92?()P\x180\x00S\xdb\x00\x00')

    def tdaq(self, msg_class=None, trans_id=0, payload_size=52):
        """Return a TdaqMessage header"""
        msg_class = msg_class or messages.FragmentRequest

        typeId = struct.pack('<i', msg_class.id)
        transId = struct.pack('<i', trans_id)
        size = struct.pack('<i', payload_size)
        return typeId + transId + size

    def data(self):
        return bytes(b'\xf0?!\x02A\x02C\x00B\x02C\x00C\x02C\x00D\x02C\x00E\x02C\x00F\x02C\x00G\x02')

    def pcapy_header(self, raw_packet, timestamp=(1, 1)):
        """Return a stub for the `Pkthdr` class."""
        # timestamp = (sec, usec)
        pkt_hdr = type('PkthdrStub', (object,), {})
        pkt_hdr.incl_len = len(raw_packet)
        pkt_hdr.ts = timestamp
        return pkt_hdr
