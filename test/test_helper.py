
import unittest

from msgdump import decoder, messages
from helper import Factory

class TestFactory(unittest.TestCase):

    def test_default_factory(self):
        """test help factories"""
        fct = Factory()
        raw_packet = fct.eth() + fct.ipv4() + fct.tcp() + fct.tdaq() + fct.data()
        self.assertEqual(96, len(raw_packet))

        pkt = decoder.decode(fct.pcapy_header(raw_packet), raw_packet)
        self.assertIs(type(pkt.msg), messages.FragmentRequest)

    def test_factory_with_params(self):
        """test factory with params build right packet"""
        fct = Factory()
        raw_packet = fct.eth()
        raw_packet += fct.ipv4(source_address='10.193.1.1',
                               destination_address='10.193.1.100')
        raw_packet += fct.tcp()
        raw_packet += fct.tdaq(messages.DcmUpdate) + fct.data()

        pkt = decoder.decode(fct.pcapy_header(raw_packet), raw_packet)
        self.assertEqual(pkt.get('source_address'), '10.193.1.1')
        self.assertEqual(pkt.get('destination_address'), '10.193.1.100')
        self.assertIs(type(pkt.msg), messages.DcmUpdate)


if __name__ == '__main__':
    unittest.main()
