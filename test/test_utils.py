#
# Test msgdump.utils
#

import unittest
from msgdump import decoder, messages, utils

from helper import Factory


class TestUtils(unittest.TestCase):
    """Test `utils` functions."""

    def test_time_diff(self):
        """test time_diff"""
        def build_helper(typ, trans_id, ts, ip):
            fct = Factory()
            raw_packet = fct.eth()
            if typ == 'Request':
                raw_packet += fct.ipv4(source_address=ip)
                cls = messages.FragmentRequest
            elif typ == 'Response':
                raw_packet += fct.ipv4(destination_address=ip)
                cls = messages.FragmentResponse

            raw_packet += fct.tcp()
            raw_packet += fct.tdaq(msg_class=cls, trans_id=trans_id) + fct.data()

            pcap_hdr = fct.pcapy_header(raw_packet, timestamp=ts)
            return decoder.decode(pcap_hdr, raw_packet)

        packets = []
        packets.append(build_helper('Request', 1000, (1, 5000), '10.193.01.01'))
        packets.append(build_helper('Response', 1000, (1, 9000), '10.193.01.01'))

        packets.append(build_helper('Request', 1000, (2, 1000), '10.193.02.02'))

        packets.append(build_helper('Request', 1020, (2, 1000), '10.193.02.02'))
        packets.append(build_helper('Response', 1020, (2, 9000), '10.193.02.02'))

        packets.append(build_helper('Response', 900, (3, 1000), '10.193.02.02'))


        r = utils.time_diff(packets,
                            incoming=messages.FragmentRequest,
                            outgoing=messages.FragmentResponse)

        self.assertEqual(2, len(r))
        self.assertAlmostEqual(4, r[0])
        self.assertAlmostEqual(8, r[1])

    def test_filter_by_address(self):
        """test filter_by_address"""
        def build_helper(source_address, destination_address):
            fct = Factory()
            raw_packet = fct.eth()
            raw_packet += fct.ipv4(source_address, destination_address)
            raw_packet += fct.tcp()
            raw_packet += fct.tdaq() + fct.data()

            pcap_hdr = fct.pcapy_header(raw_packet)
            return decoder.decode(pcap_hdr, raw_packet)

        packets = []
        packets.append(build_helper('10.193.01.01', '10.193.01.02'))
        packets.append(build_helper('10.193.01.01', '10.193.01.03'))
        packets.append(build_helper('10.193.01.03', '10.193.01.02'))
        packets.append(build_helper('10.193.01.05', '10.193.01.06'))

        lst = list(utils.filter_by_address(packets, '10.193.01.03'))
        self.assertEqual(2, len(lst))
        check = all('10.193.1.3' in (pkt.get('source_address'), pkt.get('destination_address')) for pkt in lst)
        self.assertTrue(check)

    def test_filter_by_type(self):
        """test filter_bt_type"""
        def build_helper(msg_class):
            fct = Factory()
            raw_packet = fct.eth()
            raw_packet += fct.ipv4()
            raw_packet += fct.tcp()
            raw_packet += fct.tdaq(msg_class) + fct.data()

            pcap_hdr = fct.pcapy_header(raw_packet)
            return decoder.decode(pcap_hdr, raw_packet)

        packets = []
        packets.append(build_helper(messages.FragmentResponse))
        packets.append(build_helper(messages.FragmentRequest))
        packets.append(build_helper(messages.DcmUpdate))
        packets.append(build_helper(messages.Clear))
        packets.append(build_helper(messages.FragmentRequest))
        packets.append(build_helper(messages.DcmUpdate))
        packets.append(build_helper(messages.FragmentResponse))
        
        lst = list(utils.filter_by_type(packets, messages.FragmentResponse))
        check = all(type(pkt.msg) is messages.FragmentResponse for pkt in lst)
        self.assertTrue(check)

    def test_tell_me_who(self):
        """test tell_me_who"""
        def build_helper(source_address, destination_address):
            fct = Factory()
            raw_packet = fct.eth()
            raw_packet += fct.ipv4(source_address, destination_address)
            raw_packet += fct.tcp()
            raw_packet += fct.tdaq() + fct.data()

            pcap_hdr = fct.pcapy_header(raw_packet)
            return decoder.decode(pcap_hdr, raw_packet)

        packets = []
        packets.append(build_helper('10.193.01.01', '10.193.01.02'))
        packets.append(build_helper('10.193.01.01', '10.193.01.03'))
        packets.append(build_helper('10.193.01.03', '10.193.01.01'))
        packets.append(build_helper('10.193.01.01', '10.193.01.04'))
        expected_counter = {'10.193.1.1': {'tot': 4, 'src': 3, 'dst': 1},
                            '10.193.1.2': {'tot': 1, 'src': 0, 'dst': 1},
                            '10.193.1.3': {'tot': 2, 'src': 1, 'dst': 1},
                            '10.193.1.4': {'tot': 1, 'src': 0, 'dst': 1},
                            }

        returned_counter = utils.tell_me_who(packets, verbose=False)
        self.assertEqual(returned_counter, expected_counter)


if __name__ == '__main__':
    unittest.main()
