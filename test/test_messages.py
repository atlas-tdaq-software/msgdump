#
# Test msgdump.messages
#

import unittest
from msgdump import messages


class TestMessages(unittest.TestCase):

    def test_message_repr_conversion(self):
        """test message __repr__ method"""
        msg = messages.TDaqMessage([0, 1, 0, 0, 0, 0, 0, 0])
        expected = 'TDaqMessage - transId: 1'
        self.assertEqual(repr(msg), expected)

    def test_message_str_conversion(self):
        """test message __str__ method"""
        msg = messages.TDaqMessage([0, 1, 0, 0, 0, 0, 0, 0])
        expected = 'TDaqMessage - transId: 1'
        self.assertEqual(str(msg), expected)


if __name__ == '__main__':
    unittest.main()
