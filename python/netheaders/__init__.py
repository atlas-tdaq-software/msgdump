import socket
import struct
import string

class DecodingError(Exception):
    pass

class NotEnoughData(DecodingError):
    pass

class Header(object):

    def __init__(self, pcap, offset = 0):

        self.pcap = pcap
        self.offset = offset
        self.data = {}
        self.payload_offset = self.offset

    def checksize(self, minsize):

        if (self.pcap.size - self.offset) < minsize:
            raise NotEnoughData()
        

class pcappacket(object):

    def __init__(self, size, bytes, timestamp):
        self.size = size
        self.bytes = bytes
        self.timestamp = timestamp


class Ethernet(Header):

    IPV4 = 0x800
    ARP = 0x806

    _minimum_size = 14
       
    def __init__(self, pcap, offset = 0):

        Header.__init__(self, pcap, offset)

        self.checksize(Ethernet._minimum_size)
            
        self.payload_offset += Ethernet._minimum_size

        bytes = self.pcap.bytes[offset:]
        
        self.data['destination-MAC'] = '%04x'\
                                       % socket.ntohs((struct.unpack('H',bytes[0:2])[0])) \
                                       + '%08x' \
                                       % socket.ntohl((struct.unpack('I',bytes[2:6])[0]))
        
        self.data['source-MAC'] = '%04x' \
                                  % socket.ntohs((struct.unpack('H',bytes[6:8])[0])) + '%08x' \
                                  % socket.ntohl((struct.unpack('I',bytes[8:12])[0]))
        self.data['ethertype'] = socket.ntohs((struct.unpack('H',bytes[12:14])[0]))


class IPV4(Header):
    #    * 1: Internet Control Message Protocol (ICMP)
    #    * 2: Internet Group Management Protocol (IGMP)
    #    * 6: Transmission Control Protocol (TCP)
    #    * 17: User Datagram Protocol (UDP)
    #    * 89: Open Shortest Path First (OSPF)
    #    * 132: Stream Control Transmission Protocol (SCTP)

    ICMP = socket.IPPROTO_ICMP
    IGMP = socket.IPPROTO_IGMP
    TCP = socket.IPPROTO_TCP
    UDP = socket.IPPROTO_UDP
    OSPF = 89
    SCTP = 132

    _minimum_size = 20

    def __init__(self, packet, offset=0):

        Header.__init__(self, packet, offset)

        self.checksize(IPV4._minimum_size)

        bytes = self.pcap.bytes[offset:]
        
        self.data['version'] = (bytes[0] & 0xf0) >> 4
        self.data['header_len'] = bytes[0] & 0x0f
        self.data['tos'] = bytes[1]
        self.data['total_len'] = socket.ntohs(struct.unpack('H',bytes[2:4])[0])
        self.data['id'] = socket.ntohs(struct.unpack('H',bytes[4:6])[0])
        self.data['flags'] = (bytes[6] & 0xe0) >> 5
        self.data['MF-flag'] = (bytes[6] & 0x20) >> 5
        self.data['DF-flag'] = (bytes[6] & 0x40) >> 6
        self.data['fragment_offset'] = socket.ntohs(struct.unpack('H',bytes[6:8])[0] & 0x1f)
        self.data['ttl'] = bytes[8]
        self.data['protocol'] = bytes[9]
        self.data['checksum'] = socket.ntohs(struct.unpack('H',bytes[10:12])[0])
        self.data['source_address'] = socket.inet_ntoa(bytes[12:16])
        self.data['destination_address'] = socket.inet_ntoa(bytes[16:20])
        if self.data['header_len']>5:
            self.data['options'] = bytes[20:4*(self.data['header_len']-5)]
        else:
            self.data['options'] = None

        self.payload_offset += self.data['header_len']*4


class TCP(Header):

    _minimum_size = 20

    def __init__(self, packet, offset=0):

        Header.__init__(self, packet, offset)

        self.checksize(TCP._minimum_size)

        bytes = self.pcap.bytes[offset:]

        self.data['source_port'] = socket.ntohs(struct.unpack('H',bytes[0:2])[0])
        self.data['dest_port'] = socket.ntohs(struct.unpack('H',bytes[2:4])[0])
        
        self.data['sequence_number'] = socket.ntohs(struct.unpack('H',bytes[6:8])[0]) +  (socket.ntohs(struct.unpack('H',bytes[4:6])[0]) << 16)
       
        self.data['ack_number'] = socket.ntohs(struct.unpack('H',bytes[10:12])[0]) +  (socket.ntohs(struct.unpack('H',bytes[8:10])[0]) << 16)
        self.data['header_len'] = (bytes[12] & 0xf0) >> 4
        self.data['flags'] = bytes[13]
        self.data['CWR'] = (bytes[13] & 0x80) >> 4
        self.data['ECE'] = (bytes[13] & 0x40) >> 4
        self.data['URG'] = (bytes[13] & 0x20) >> 4
        self.data['ACK'] = (bytes[13] & 0x10) >> 4
        self.data['PSH'] = (bytes[13] & 0x8)
        self.data['RST'] = (bytes[13] & 0x4)
        self.data['SYN'] = (bytes[13] & 0x2)
        self.data['FIN'] = (bytes[13] & 0x1)
        self.data['window_size'] = socket.ntohs(struct.unpack('H',bytes[14:16])[0])
        self.data['checksum'] = socket.ntohs(struct.unpack('H',bytes[16:18])[0])
        self.data['urgent_pointer'] = socket.ntohs(struct.unpack('H',bytes[18:20])[0])
        if self.data['header_len']>5:
            self.data['options'] = bytes[20:4*(self.data['header_len']-5)]
        else:
            self.data['options'] = None

        self.payload_offset += self.data['header_len']*4


class UDP(Header):

    _minimum_size = 8

    def __init__(self, packet, offset=0):

        Header.__init__(self, packet, offset)

        self.checksize(UDP._minimum_size)

        bytes = self.pcap.bytes[offset:]

        self.data['source_port'] = socket.ntohs(struct.unpack('H',bytes[0:2])[0])
        self.data['dest_port'] = socket.ntohs(struct.unpack('H',bytes[2:4])[0])
        self.data['total_length'] = socket.ntohs(struct.unpack('H',bytes[4:6])[0])
        self.data['checksum'] = socket.ntohs(struct.unpack('H',bytes[6:8])[0])

        self.payload_offset += UDP._minimum_size



def dumphex(s):
  bytes = map(lambda x: '%.2x' % x, map(ord, s))
  for i in range(0,len(bytes)/16):
    print('    %s' % string.join(bytes[i*16:(i+1)*16],' '))
    
  print('    %s' % string.join(bytes[(len(bytes)/16)*16:],' '))



def char2number(s):

    numbers = []
    
    #for i in range(0,len(s)//4):
    #    numbers.append(socket.ntohl(struct.unpack('I',s[4*i:4*i+4])[0]))

    #if len(s)%4 != 0:
    #    numbers.append(socket.ntohs(struct.unpack('H',s[len(s)-2:])[0]))

    for i in range(0, len(s)//4):
        numbers.append(struct.unpack('I',s[4*i:4*i+4])[0])
        
    if len(s)%4 != 0:
        numbers.append(struct.unpack('H',s[len(s)-2:])[0])

    return numbers
    
#if __name__ == '__main__':

#    pcapob = pcap.pcapObject()
#    pcapob.open_offline('sfi-to-pros-dump.pcap')

#    packets = []
    
#    while True:
#        try:
#            (l,b,t) = pcapob.next()
#        except TypeError:
#            break
#        
 #       packet = pcappacket(l,b,t)
 #       ethehead = ethernetheader(packet.bytes)##

#        if not ethehead.data['ethertype'] == ethehead.IPV4:
#            print 'Not a IPV4 fragment ... skipping'
#            continue

#        ipvhead = IPV4header(ethehead.payload)

#        if not ipvhead.data['protocol'] == ipvhead.TCP:
#            print 'Not a TCP fragment ... skipping'
#            continue

#        tcphead = TCPheader(ipvhead.payload)
        
#        packets.append([packet,ethehead,ipvhead,tcphead])
    
        #break
