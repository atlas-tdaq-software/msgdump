#
# msgdump/decoder.py
#
"""
This module provides the decoding functionalities, mainly through the
:func:`load` function. For example::

    >>> from msgdump import decoder
    >>> for pkt in decoder.load("file.pcap"):
    ...     print pkt.msg

Where `pkt` is a :class:`DecodedPacket`.

"""

__all__ = ['decode', 'load', 'DecodedPacket', 'DecodingError', 'WrongFragment']

from netheaders import DecodingError


class WrongFragment(DecodingError):
    pass


class DecodedPacket(object):
    """High level packet class."""

    def __init__(self, headers, msg, timestamp):
        self.headers = headers
        self.msg = msg
        self.timestamp = timestamp

    def get(self, key, default=None):
        for name,h in self.headers.items():
            try:
                return h.data[key]
            except KeyError:
                pass
        return default


def decode(hdr, data):
    """Return a :class:`DecodedPacket` given a pcap header `hdr` and raw bytes
    `data`.

    """
    from netheaders import Ethernet, IPV4, TCP, UDP, pcappacket, char2number
    from msgdump.messages import message_register, EmptyMessage, UndefinedMessage

    # Hiding netheaders here:
    timestamp = hdr.ts[0] + hdr.ts[1]/1000000.
    packet = pcappacket(hdr.incl_len, data, timestamp)

    headers = {}
    ethehead = Ethernet(packet)
    headers['ETH'] = ethehead

    if not ethehead.data['ethertype'] == ethehead.IPV4:
        raise WrongFragment('Not a IPV4 fragment', ethehead.data['ethertype'])

    ipvhead = IPV4(packet, ethehead.payload_offset)
    headers['IPV4'] = ipvhead

    if ipvhead.data['protocol'] == ipvhead.TCP:
        tcphead = TCP(packet, ipvhead.payload_offset)
        headers['TCP'] = tcphead
        seq = char2number(packet.bytes[tcphead.payload_offset:])

    elif ipvhead.data['protocol'] == ipvhead.UDP:
        udphead = UDP(packet, ipvhead.payload_offset)
        headers['UDP'] = udphead
        seq = char2number(packet.bytes[udphead.payload_offset:])

    else:
        raise WrongFragment('Neither TCP or UDP fragment', ipvhead.data['protocol'])

    if not seq:
        msg = EmptyMessage()
    else:
        msg = message_register.get(seq[0], UndefinedMessage)(seq)

    return DecodedPacket(headers, msg, timestamp=packet.timestamp)


def _load(filename, max_packets=1000000):
    """Open a pcap file and yield a `max_packets` number of
    :class:`DecodedPacket`\s.

    """
    import pure_pcapy

    pcap_reader = pure_pcapy.open_offline(filename)

    packet_count = 0
    while packet_count < max_packets:
        packet_count += 1

        hdr, data = pcap_reader.next()
        if hdr is None:
            break

        try:
            yield decode(hdr, data)
        except WrongFragment as e:
            continue

def load(filename, max_packets=1000000):
    """Open a pcap file and return a list of :class:`DecodedPacket`\s."""
    return list(_load(filename, max_packets))

if __name__ == '__main__':
    import sys

    for pkt in load(sys.argv[1]):
        print(pkt.msg)
