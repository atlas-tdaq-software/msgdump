#
# msgdump/utils.py
#
"""
This module implements some utility functionalities and analyzing routines.

"""

__all__ = ['time_diff', 'filter_by_address', 'filter_by_type', 'tell_me_who']

from msgdump import decoder, messages


def time_diff(packets, incoming, outgoing):
    """Given a list of `DecodedPacket`\s and two message types `incoming` and
    `outgoing` it returns a list with the differences between timestamps of
    those with the same :attr:`transId`.

    Example::

        >>> from msgdump import decoder, messages, utils
        >>> packets = decoder.load("file.pcap")
        >>> utils.time_diff(packets, messages.FragmentRequest, messages.FragmentResponse)
        [0.01, 0.02, ... ]

    """
    from collections import defaultdict

    events = defaultdict(list)

    for pck in packets:
        if pck.msg.type() is incoming:
            events[(pck.get('source_address'), pck.msg.transId)].append(pck)
        elif pck.msg.type() is outgoing:
            events[(pck.get('destination_address'), pck.msg.transId)].append(pck)

    diffs = [(e[1].timestamp - e[0].timestamp)*1000. for e in events.values() if len(e) == 2]
    return diffs


def filter_by_address(packets, address):
    """Yield from `packets` those that are either coming or going from/to the
    given `address`.

    """
    from socket import inet_aton
    address = inet_aton(address)
    for pkt in packets:
        if address in map(inet_aton, (pkt.get('source_address'), pkt.get('destination_address'))):
            yield pkt


def filter_by_type(packets, msg_type):
    """Yield from `packets` those that match `msg_type`."""
    for pkt in packets:
        if pkt.msg.type() is msg_type:
            yield pkt


def tell_me_who(packets, verbose=False):
    """Return a dictionary where to an IP address corresponds a counter
    (another dictionary) of the occurrences for that address. If `verbose` is
    set to `True` the function will print the result with a formatted output.

    Example::

        >>> from msgdump import decoder, messages, utils
        >>> packets = decoder.load("file.pcap")
        >>> utils.tell_me_who(packets)
        {'10.193.01.01': {'tot': 3, 'src': 2, 'dst': 1}, ...}

    """
    from collections import defaultdict

    counter = defaultdict(lambda: {'tot': 0, 'src': 0, 'dst': 0})

    for pkt in packets:
        counter[pkt.get('source_address')]['src'] += 1
        counter[pkt.get('destination_address')]['dst'] += 1

        counter[pkt.get('destination_address')]['tot'] += 1
        counter[pkt.get('source_address')]['tot'] += 1

    if verbose:
        for k,v in counter.iteritems():
            print('{addr} -> #tot: {tot} - #src: {src} - #dst: {dst}'.format(
                addr=k, **v))

    return counter
