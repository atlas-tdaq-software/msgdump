#
# msgdump/__init__.py
#
"""
:mod:`msgdump` is a tool written in Python for decoding pcap files into more
high level messages as described in the
`DataflowEvolution <https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DataflowEvolution>`_
> `MessagePassing <https://twiki.cern.ch/twiki/bin/viewauth/Atlas/MessagePassing>`_.

"""
__version__ = '0.0.2'

from msgdump import decoder, messages, utils
