#
# msgdump/messages.py
#
"""
This module implements the message definitions with the following class
hierarchy structure::

    EmptyMessage
     +-- UndefinedMessage
     +-- TDaqMessage
          +-- DcmUpdate
          +-- L1ResultAssign
          |    +-- L1ResultReassign
          +-- Clear
          +-- FragmentRequest
          +-- EventAssignment
          +-- SfoUpdate
          +-- EventTransfer

"""

__all__ = [
     'EmptyMessage', 'UndefinedMessage', 'TDaqMessage', 'DcmUpdate',
     'L1ResultAssign', 'L1ResultReassign', 'Clear', 'FragmentRequest',
     'EventAssignment', 'SfoUpdate', 'EventTransfer', 'message_register'
]


class MessageRegister(object):
    """Register for holding the id <-> message correspondence."""

    def __init__(self):
        self._msg2id = {}
        self._id2msg = {}

    def __repr__(self):
        return '{0}()'.format(self.__class__.__name__)

    def register(self, msg_class):
        """Register a `msg_class`."""
        self._msg2id[msg_class] = msg_class.id
        self._id2msg[msg_class.id] = msg_class

    def get(self, id, default=None):
        """Return the corrispondent message class for a given `id`."""
        return self._id2msg.get(id, default)

#: MessageRegister object holding references to the message classes.
message_register = MessageRegister()


class EmptyMessage(object):
    """Empty message."""

    id = None

    def __init__(self):
        super(EmptyMessage, self).__init__()

    def type(self):
        return type(self)


class UndefinedMessage(EmptyMessage):
    """Undefined message, (not a :class:`TDaqMessage`)."""

    def __init__(self, sequence):
        super(UndefinedMessage, self).__init__()
        self.content = sequence


class TDaqMessage(EmptyMessage):
    """Base message class implementing the headers defined in the
    `MessagePassing <https://twiki.cern.ch/twiki/bin/viewauth/Atlas/MessagePassing>`_

    """
    def __init__(self, sequence):
        super(TDaqMessage, self).__init__()
        self.typeId = sequence[0]
        self.transId = sequence[1]
        self.size = sequence[2]
        self.payload = sequence[3:]

    def __repr__(self):
        return '{0} - transId: {1}'.format(self.type().__name__, self.transId)


class DcmUpdate(TDaqMessage):
    id = 0x00DCDF00

    def numReq(self):
        return self.payload[0]

    def L1Ids(self):
        return self.payload[1:]

message_register.register(DcmUpdate)


class L1ResultAssign(TDaqMessage):
    id = 0x00DCDF01

    def GIDlo(self):
        return self.payload[0]

    def GIDup(self):
        return self.payload[1]

    def L1Id(self):
        return self.payload[2]

    def L1Results(self):
        return self.payload[3:]

message_register.register(L1ResultAssign)


class L1ResultReassign(L1ResultAssign):
    id = 0x00DCDF0F

message_register.register(L1ResultReassign)


class Clear(TDaqMessage):
    id = 0x00DCDF10

    def L1Ids(self):
        return self.payload

message_register.register(Clear)


class FragmentRequest(TDaqMessage):
    id = 0xDCDF20

    def L1Id(self):
        return self.payload[0]

    def ROBIds(self):
        return self.payload[1:]

message_register.register(FragmentRequest)


class FragmentResponse(TDaqMessage):
    id = 0xDCDF21

    def ROBData(self):
        return self.payload

message_register.register(FragmentResponse)


class EventAssignment(TDaqMessage):
    id = 0x00DCDF40

    def nEvents(self):
        return self.payload[0]

message_register.register(EventAssignment)


class SfoUpdate(TDaqMessage):
    id = 0x00DCDF41

    def numReq(self):
        return self.payload[0]

    def GIDlos(self):
        return self.payload[1::2]

    def GIDups(self):
        return self.payload[2::2]

message_register.register(SfoUpdate)


class EventTransfer(TDaqMessage):
    id = 0x00DCDF42

    def EventData(self):
        return self.payload

message_register.register(EventTransfer)
