#!/bin/bash
# Created by Riccardo Poggi <riccardo.poggi@cern.ch>
# Wed 28 Aug 2013 15:01:45 PM CET
export PYTHONPATH=../python:${TDAQ_PYTHONPATH}
tdaq_python ../test/test_all.py
exit $?
